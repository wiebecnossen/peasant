namespace Peasant {
  using System;
  using System.Runtime.CompilerServices;

  using static System.Math;

  public class RussianPeasant {
    private readonly Func<ulong, ulong, ulong> mul;
    private readonly Func<ulong, ulong, ulong> pow;
    private int additions;
    private int halves;

    public RussianPeasant() {
      this.mul = this.Power(this.Add, 0);
      this.pow = this.Power(this.Mul, 1);
    }

    public ulong Add(ulong a, ulong b) {
      this.additions++;
      return a + b;
    }

    public ulong Mul(ulong a, ulong b) {
      return this.mul(Max(a, b), Min(a, b));
    }

    public ulong Pow(ulong a, ulong b) {
      return this.pow(a, b);
    }

    public string Stats() {
      return $"+{this.additions} >>{this.halves}";
    }

    private Func<ulong, ulong, ulong> Power(Func<ulong, ulong, ulong> func, ulong identity) {
      static bool Odd(ulong operand1) {
        return (operand1 & 1) == 1;
      }

      return (a, b) => {
        var (power, todo, result) = (a, b, identity);
        while (todo > 0) {
          if (Odd(todo)) {
            result = func(result, power);
          }

          power = func(power, power);
          todo = this.Half(todo);
        }

        return result;
      };
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private ulong Half(ulong a) {
      this.halves++;
      return a >> 1;
    }
  }
}
