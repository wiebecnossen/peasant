﻿namespace Peasant {
  using System;
  using System.Linq;
  using System.Text.RegularExpressions;

  internal static class Program {
    private static void Main() {
      Console.WriteLine($"Welcome to the {nameof(Peasant)} world!");
      while (true) {
        Console.Write($"{nameof(Peasant)}> ");
        var line = Console.ReadLine() ?? string.Empty;
        var words = Regex.Split(line.Replace(" ", string.Empty), @"\b")
          .Where(s => !string.IsNullOrWhiteSpace(s))
          .ToArray();
        if (!words.Any()) {
          continue;
        }

        if (words[0] == "exit" || words[0] == "quit") {
          break;
        }

        if (words.Length < 3) {
          continue;
        }

        if (!ulong.TryParse(words[0], out var a) || !ulong.TryParse(words[2], out var b)) {
          continue;
        }

        try {
          var peasant = new RussianPeasant();
          ulong result;
          switch (words[1]) {
            case "+":
              result = peasant.Add(a, b);
              break;
            case "*":
              result = peasant.Mul(a, b);
              break;
            case "^":
              result = peasant.Pow(a, b);
              break;
            default:
              continue;
          }

          Console.WriteLine($"{$"{line} = {result}",-60} {peasant.Stats(),18}");
        }
        catch (OverflowException) {
          Console.WriteLine($"Too big for this poor {nameof(Peasant)}!");
        }
      }
    }
  }
}
